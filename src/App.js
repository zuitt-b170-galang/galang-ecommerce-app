import React, { useState } from 'react';

import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

import 'bootstrap/dist/css/bootstrap.min.css';
// import './index.css';

import UserContext from './userContext.js';

import AppNavbar from './components/AppNavbar.js';

import Home from './pages/Home.js';
// import Products from './pages/Products';
import Register from './pages/Register';
import Login from './pages/Login';
import Error from './pages/Error';


export default function App() {

	const [ user, setUser ] = useState( { accessToken: localStorage.getItem('access') } );

	const unsetUser = () => {
		localStorage.clear();
		setUser( { access: null } )
	}


	return (
		<UserContext.Provider value={{ user, setUser, unsetUser }}>
			 <Router>
			   <AppNavbar user={user} />
			   <Routes>
			       <Route path = "/" element={<Home />} />
			       {/*<Route path = "/products" element = {<Products />} />*/}
			       <Route path = "/register" element = {<Register />} />
			       <Route path = "/login" element = {<Login />} />
			       <Route path="*" element = {<Error />} />
			   </Routes>
			</Router>
		</UserContext.Provider>
		)
}
