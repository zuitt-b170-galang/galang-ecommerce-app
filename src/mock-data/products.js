export default [
	{
		id: "ksm1",
		name: "Twice Formula of Love: O+T=<3 Album",
		description: "Third Full Album Released 11/12/21. Includes CD, Photobook, 4 Photocards, 1 Poster.",
		price: 1250,
		onOffer: true
	},
	{
		id: "ksm2",
		name: "IM NAYEON Pre-Order",
		description: "First Solo Debut Twice Nayeon. Release 06/24/22. Includes CD, Photobook, 2 Photocards.",
		price: 890,
		onOffer: true
	},
	{
		id: "ksm3",
		name: "itzy Photocard Set",
		description: "itzy lomo cards. (8.5cm*5.4cm) 54pc/box",
		price: 500,
		onOffer: true
	}
]
