import React, { Fragment, useContext } from 'react';
import { Link, NavLink, useNavigate } from 'react-router-dom';

import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

import UserContext from '../userContext.js'

export default function AppNavbar(){

  const { user, unsetUser } = useContext(UserContext);
  const navigate = useNavigate();

  const logout = () => {
    unsetUser();
    navigate('/login');
  }

  let rightNav = (user.access === null) ? (
      <Fragment>
        <Nav.Link as={NavLink} to='/register'>Register</Nav.Link>
        <Nav.Link as={NavLink} to='/login'>Login</Nav.Link>
      </Fragment>
    ) : (
      <Fragment>
        <Nav.Link onClick={logout}>Logout</Nav.Link>
      </Fragment>
    )

  return(
      <Navbar bg='light' expand='lg'>
        <Navbar.Brand as={Link} to='/'>Kpop Seoul Merch</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
                <Nav.Link as={NavLink} to='/'>Home</Nav.Link>
                {/*<Nav.Link as={NavLink} to='/courses'>Products</Nav.Link>*/}
            </Nav>
            <Nav className = 'ml-auto'>
              {rightNav}
            </Nav>
        </Navbar.Collapse>
      </Navbar>
    )
}
