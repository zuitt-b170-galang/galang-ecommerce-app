import React, { useState, useEffect } from 'react';

import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';

export default function Product(props) {

    let product = props.product;

    const [ isDisabled, setIsDisabled ] = useState(0);
    const [ stocks, setStocks ] = useState(10);

    useEffect(()=> {
        if (stocks === 0) {
            setIsDisabled(true);
        }
    },[stocks]);

    return (
        <Card>
            <Card.Body>
                <Card.Title>{product.name}</Card.Title>
                <h6>Description:</h6>
                <p>{product.description}</p>
                <h6>Price:</h6>
                <p>{product.price}</p>
                <h6>Stock:</h6>
                <p>{stocks} remaining</p>
                <Button variant="primary" onClick={() => setStocks(stocks - 1)} disabled={isDisabled}>Enroll</Button>
            </Card.Body>
        </Card>
    )
}
