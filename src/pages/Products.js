import React from 'react';
import Product from '../components/Product';

import Container from 'react-bootstrap/Container';

import products from '../mock-data/products';


export default function Products() {
	const ProductCards = productss.map((product) => {
		return (
				<Product product={product} />
			);
	});
	return (
			<Container fluid>
				{ProductCards}
			</Container>
		)
}
