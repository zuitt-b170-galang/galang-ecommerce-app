import React, { useState, useEffect, useContext } from 'react';
import { Navigate } from 'react-router-dom';

import UserContext from '../userContext'

import { Form, Container, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function Login() {

    const { user, setUser } = useContext(UserContext);

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [ isDisabled, setIsDisabled ] = useState(true);

    useEffect(()=>{
        let isEmailNotEmpty = email !== '';
        let isPasswordNotEmpty = password !== '';

        if ( isEmailNotEmpty && isPasswordNotEmpty ) {
            setIsDisabled(false);
        } else {
            setIsDisabled(true);
        }
        },[ email, password ]);

    function login(e) {
        e.preventDefault();

        fetch('http://localhost:4000/api/users/login', {
            method: "POST",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify( {
                email: email, 
                password: password
                } )
        })
        .then((response) => response.json())
        .then((response) => {
            if (response.access !== undefined) {
                Swal.fire('You are logged in')
                localStorage.setItem('access', response.access);
                setUser( { access: response.access } );
            }
            else {
                alert(response.error);
                setEmail('');
                setPassword('');
            }
        })
    }    

    if (user.access !== null) {
        return <Navigate replace to="/" />
    }

    return (
        <Container fluid>
            <h3>Login</h3>
            <Form onSubmit={login}>
                <Form.Group>
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" value={email} onChange={(e) => setEmail(e.target.value)} required/>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)} required/>
                </Form.Group>
                <Button variant="success" type="submit" disabled={isDisabled}>Login</Button>
            </Form>
        </Container>
    )
}
