import	React, { useState, useEffect, useContext } from 'react';
import	{ Navigate } from 'react-router-dom';

import UserContext from '../userContext.js';

import { Form, Container, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function Register(){

	const { user, setUser } = useContext(UserContext);

	const [ firstName, setFirstName ] = useState('');
	const [ lastName, setLastName ] = useState('');
	const [ email, setEmail ] = useState('');
	const [ password, setPassword ] = useState('');
	const [ passwordConfirm, setPasswordConfirm ] = useState('');
	const [ isDisabled, setIsDisabled ] = useState(true);
	const [ isDisabled, setIsDisabled ] = useState(true);

	useEffect(()=>{
		let isFirstNameNotEmpty = firstName !== '';
		let isLastNameNotEmpty = lastName !== '';
		let isEmailNotEmpty = email !== '';
		let isPasswordNotEmpty = password !== '';
		let isPasswordConfirmNotEmpty = passwordConfirm !== '';
		let isPasswordMatch = password === passwordConfirm;

		if ( isFirstNameNotEmpty && isLastNameNotEmpty && isEmailNotEmpty && isPasswordNotEmpty && isPasswordConfirmNotEmpty && isPasswordMatch ) {
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}

	},[ firstName, lastName, email, password]);


	function register(e){
		e.preventDefault();

		
				Swal.fire('Register successful, you may now log in.')

                setFirstName('');
				setLastName('');
				setEmail('');
				setPassword('');

	}



	
	return(
			<Container>
				<Form onSubmit={register}>
					<Form.Group>
						<Form.Label>First Name</Form.Label>
						<Form.Control type="string" placeholder="Enter first name" value={firstName} onChange={(e) => setFirstName(e.target.value)} required />
					</Form.Group>
					<Form.Group>
						<Form.Label>Last Name</Form.Label>
						<Form.Control type="string" placeholder="Enter last name" value={lastName} onChange={(e) => setLastName(e.target.value)} required />
					</Form.Group>
					<Form.Group>
						<Form.Label>Email address</Form.Label>
						<Form.Control type="email" placeholder="Enter email" value={email} onChange={(e) => setEmail(e.target.value)} required />
						<Form.Text className='text-muted'>We'll never share your email to anyone else</Form.Text>
					</Form.Group>
					<Form.Group>
						<Form.Label>Password</Form.Label>
						<Form.Control type="password" placeholder="Enter password" value={password} onChange={(e) => setPassword(e.target.value)} required />
					</Form.Group>
					<Button variant="primary" type="submit" disabled={isDisabled}>Submit</Button>
				</Form>
			</Container>
		)
}
