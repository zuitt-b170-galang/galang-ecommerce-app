import	React, { Fragment } from 'react';

import Banner from '../components/Banner.js';

export default function Home() {

	const data = {
	    title: "Kpop Seoul Merch",
	    content: "Your one stop Kpop shop",
	    // destination: "/products",
	    label: "Our Products"
	}

	return (
		<Fragment>
			<Banner data={data}/>
		</Fragment>
	)
}
